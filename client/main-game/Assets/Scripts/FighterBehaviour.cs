using Fusion;
using UnityEngine;

public class FighterBehaviour : NetworkBehaviour
{
    [SerializeField]
    private NetworkCharacterController _characterController;

    [SerializeField] private Animator _animator;

    [SerializeField] 
    private int _moveSpeed = 5;

    private readonly int _moveHash = Animator.StringToHash("move");
    private readonly int _attackHash = Animator.StringToHash("attack");
    
    public override void FixedUpdateNetwork()
    {
        if (GetInput(out NetworkInputData data))
        {
            data.direction.Normalize();

            if (data.direction != Vector3.zero)
            {
                _characterController.transform.forward = data.direction;
                _characterController.Move(_moveSpeed * data.direction * Runner.DeltaTime);
                _animator.SetBool(_moveHash, true);
            }
            else
            {
                _animator.SetBool(_moveHash, false);
            }

        }
        

    }
}
